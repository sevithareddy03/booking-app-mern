import "./Register.css";
import axios from "axios";
import { Link } from "react-router-dom";
import { useContext, useState } from "react";
import { AuthContext } from "../../context/AuthContext";

const Register = () => {

    const { loading, error, dispatch } = useContext(AuthContext);

    const [registerdata, setRegisterdata] = useState({
        username: undefined,
        email:undefined,
        password: undefined,
        country:undefined,
      });

    const handleChange = (e) => {
        setRegisterdata((prev) => ({ ...prev, [e.target.id]: e.target.value }));
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const res = await axios.post("/auth/register", registerdata);
            res.data && window.location.replace("/login");
        } 
        catch (err) {
            dispatch({ type: "LOGIN_FAILURE", payload: err.response.data });
        }
    };

    return (
    <div className="register">
        <div className="rContainer">
        <Link to="/" style={{ color: "inherit", textDecoration: "none" }}>
        <span className="rlogo">Sevitha Reservations</span>
      </Link>
            <input
            type="text"
            placeholder="Username"
            id="username"
            onChange={handleChange}
            className="rInput"
            />
            <input
                type="email"
                placeholder="Email"
                id="email"
                onChange={handleChange}
                className="rInput"
            />
            <input
                type="password"
                placeholder="Password"
                id="password"
                onChange={handleChange}
                className="rInput"
            />
            <input
                type="text"
                placeholder="Country"
                id="country"
                onChange={handleChange}
                className="rInput"
            />
            <button disabled={loading} onClick={handleSubmit} className="rButton">
                Register
            </button>
            {error && <span>{error.message}</span>}
            <div classname="rAccount" >
                Already have an account? 
                <Link className="link" to="/login">
                    Login
                </Link>
            </div>
        </div>
    </div>
    );
};
export default Register;