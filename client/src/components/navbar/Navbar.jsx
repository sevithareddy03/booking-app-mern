import { Link, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import "./Navbar.css";

const Navbar = () => {

  const navigate = useNavigate();
  const { user , dispatch} = useContext(AuthContext);


  const handleLogout = () => {
    dispatch({ type: "LOGOUT" });
  };

  const handleLogin = () => {
    navigate("/login");
  };

  const handleRegister = () => {
    navigate("/register");
  };

    return(
    <div className="navbar">
      <div className="navContainer">
      <Link to="/" style={{ color: "inherit", textDecoration: "none" }}>
        <span className="logo">Sevitha Reservations</span>
      </Link>
      {user ? 
      (
        <div className="navItems">
        <span className="username">{user.username}</span>
        <button className="navButton" onClick={handleLogout}>Logout</button>
        </div>
      )
        : 
      (
          <div className="navItems">
            <button className="navButton" onClick={handleRegister}>Register</button>
            <button className="navButton" onClick={handleLogin}>Login</button>
          </div>
        )}
      </div>
    </div>
    );
};

export default Navbar;